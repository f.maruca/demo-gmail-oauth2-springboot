package com.example.demo;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.GmailScopes;

import com.google.code.samples.oauth2.OAuth2Authenticator;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.api.client.http.javanet.NetHttpTransport;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.*;
import java.util.Collections;
import java.util.List;

@Component
public class ImapConnectionCustom {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PropertiesCustom propertiesCustom;

	//private static final String APPLICATION_NAME = "Gmail API Demo";
	private static final JsonFactory JSON_FACTORY = new JacksonFactory();

	private static final List<String> SCOPES = Collections.singletonList(GmailScopes.MAIL_GOOGLE_COM);

	private String authToken;




    public void start() throws Exception {

		// Step 1: Get Gmail Credencial Object
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		Credential credential = getCredentials(HTTP_TRANSPORT);


		// Step 2: Get Access Token From Gmail Credencial Object
		authToken = credential.getAccessToken();


		// Step 3: Open Imap connection
		OAuth2Authenticator.initialize();

		IMAPStore store = OAuth2Authenticator.connectToImap("imap.gmail.com",
				993,
				propertiesCustom.getGmailEmail(),
				authToken,
				true);


		// Step 4: Read Email
		this.getEmail(store);


    }


	private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws Exception {
		// Load client secrets.
		InputStream in = this.getClass().getResourceAsStream(propertiesCustom.getCredentialFilePath());

		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + propertiesCustom.getCredentialFilePath());
		}

		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
				.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(propertiesCustom.getTokenDirectoryPath())))
				.setAccessType("offline")
				.build();

		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();

		AuthorizationCodeInstalledApp authorizationCodeInstalledApp = new AuthorizationCodeInstalledApp(flow, receiver);

		Credential credential = authorizationCodeInstalledApp.authorize(clientSecrets.getDetails().getClientId());

		return credential;
	}


	private void getEmail(IMAPStore store) {
		try{
			final IMAPFolder folder = (IMAPFolder) store.getFolder("Inbox");
			folder.open(Folder.READ_ONLY);

			Message message[] = folder.getMessages();

			log.info("");
			log.info("");
			log.info("");
			log.info("");
			log.info(">>>>>>>>>>>>>>>>>>>>> Email - START <<<<<<<<<<<<<<<<<<");
			log.info(">>>> Number to email: " + message.length);
			log.info("");
			log.info("");
			log.info("");
			log.info("");

			for (int i=0, n=message.length; i<n; i++){

				String from = message[i].getFrom()[0].toString();
				String to = message[i].getRecipients(Message.RecipientType.TO)[0].toString();
				String subject = message[i].getSubject().toString();

				log.info("");
				log.info("");
				log.info("");
				log.info("");
				log.info(">>>>>>>>> Email " + String.valueOf(i));
				log.info(">>>> From: " + from);
				log.info(">>>> To: " + to);
				log.info(">>>> Subject: " + subject );
				log.info("");
				log.info("");
				log.info("");
				log.info("");

			}

			log.info("");
			log.info("");
			log.info("");
			log.info("");
			log.info(">>>>>>>>>>>>>>>>>>>>> Email - STOP <<<<<<<<<<<<<<<<<<");
			log.info("");
			log.info("");
			log.info("");
			log.info("");

			folder.close(false);
			store.close();

			}catch(Exception e){
				e.printStackTrace();
			}

	}



}
