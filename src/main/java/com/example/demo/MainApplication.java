package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.example.demo")
@PropertySource("classpath:application.properties")
public class MainApplication implements CommandLineRunner {

	@Autowired
	private ImapConnectionCustom imapConnectionCustom;

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	public static void main(String[] args) {
		SpringApplication.run(MainApplication.class, args);



	}

	@Override
	public void run(String... args) throws Exception {
		imapConnectionCustom.start();
	}


}